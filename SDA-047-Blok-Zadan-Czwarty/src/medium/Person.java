package medium;

import java.util.Random;

// Zadanie 8

public class Person {

	private static String[] myArray1 = { "Kr", "Ca", "Ra", "Mrok", "Cru", "Ray", "Bre", "Zed", "Drak", "Mor", "Jag",
			"Mer", "Jar", "Mjol", "Zork", "Mad", "Cry", "Zur", "Creo", "Azak", "Azur", "Rei", "Cro", "Mar", "Luk" };
	private static String[] myArray2 = { "air", "ir", "mi", "sor", "mee", "clo", "red", "cra", "ark", "arc", "miri",
			"lori", "cres", "mur", "zer", "marac", "zoir", "slamar", "salmar", "urak" };
	private static String[] myArray3 = { "d", "ed", "ark", "arc", "es", "er", "der", "tron", "med", "ure", "zur",
			"cred", "mur" };

	public static void randomPerson() {

		System.out.println(myArray1[new Random().nextInt(myArray1.length)] + myArray2[new Random().nextInt(myArray2.length)] + myArray3[new Random().nextInt(myArray3.length)]);

	}

	public static void main(String[] args) {

		randomPerson();
		
	}

}
