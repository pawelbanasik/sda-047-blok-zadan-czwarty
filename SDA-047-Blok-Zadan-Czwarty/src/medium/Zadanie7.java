package medium;

// Zadanie 7 (bez metody)
// double[] myArray = { 1, 2, 3, 4, 5, 6, 1, 1, 1 };
// for (int i = 0; i < myArray.length; i++) {
//
// myArray[i] = Math.pow(myArray[i], 2);
//
// }
//
// for (double elements : myArray) {
// System.out.println(elements);
// }

public class Zadanie7 {
	private double[] myArray;

	public Zadanie7(double[] myArray) {
		this.myArray = myArray;
	}

	public static void powerArray(double[] myArray) {
		for (int i = 0; i < myArray.length; i++) {

			myArray[i] = Math.pow(myArray[i], 2);
		}
		for (double elements : myArray) {
			System.out.println(elements);

		}

	}

	public static void main(String[] args) {
		double[] Array1 = { 1, 2, 3, 4, 5, 6, 1, 1, 1 };

		powerArray(Array1);

	}

}
