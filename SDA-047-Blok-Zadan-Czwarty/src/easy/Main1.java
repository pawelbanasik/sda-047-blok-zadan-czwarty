package easy;

import java.util.Scanner;

public class Main1 {

	public static void main(String[] args) {

		// Zadanie 1
		// System.out.println("Hello World");

		// Zadnie 2
		// for (int i = 1; i < 6; i++) {
		//
		// System.out.println("Hello world " + i);
		// }

		// Zadanie 3
		// Scanner sc = new Scanner(System.in);
		// System.out.println("Podaj liczbe a: ");
		// int a = sc.nextInt();
		// System.out.println("Podaj liczbe b: ");
		// int b = sc.nextInt();
		// if (a > b) {
		// System.out.println("Wieksza liczba jest liczba a: " + a);
		// }
		// else {
		// System.out.println("Wieksza liczba jest liczba b: " + b);
		// }

		// Zadanie 4
		// Scanner sc = new Scanner(System.in);
		// System.out.println("Podaj imie");
		// String imie = sc.next();
		// System.out.println("Podaj nazwisko");
		// String nazwisko = sc.next();
		// System.out.println("Witaj " + imie + " " + nazwisko);

		// Zadanie 5
		// sposob 1 (for each loop)
		int[] myArray = { 0, 1, 4, 3, 5, 5, 7, 9 };
		for (int elementy : myArray) {
			System.out.println(elementy);
		}

		System.out.println();

		// sposob 2 (for loop)
		for (int i = 0; i < myArray.length; i++) {
			System.out.println(myArray[i]);
		}
	}

}
